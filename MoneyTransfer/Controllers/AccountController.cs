﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Models;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ApplicationContext _context;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, ApplicationContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            var chars = "0123456789";
            var stringChars = new char[6];
            Random rnd = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[rnd.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            int personalAcc;
            if (ModelState.IsValid)
            {
                User user = new User { Email = model.Email, UserName = model.Email };
                User user2 = new User();
                do
                {
                    personalAcc = Int32.Parse(finalString);
                    user2 = _context.Users.FirstOrDefault(p => p.PersonalAccount == personalAcc);
                }
                while (user2 != null);
                user.PersonalAccount = personalAcc;
                user.Balanse = 1000;
                // добавляем пользователя

                _context.SaveChanges();
                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    // установка куки
                    await _signInManager.SignInAsync(user, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View("PersonalArea");
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = _context.Users.FirstOrDefault(u =>
                    u.Email == model.Email || u.PersonalAccount.ToString() == model.Email);
                var result =
                    await _signInManager.PasswordSignInAsync(user.Email, model.Password, model.RememberMe, false);
                if (result.Succeeded)
                {
                    // проверяем, принадлежит ли URL приложению
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            // удаляем аутентификационные куки
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult PersonalArea(string email)
        {
            User user = _context.Users.FirstOrDefault(u => u.Email == email);
            return View("PersonalArea", user);
        }
        

        [HttpPost]
        public IActionResult Replenishment(int PersonalAccount, double Sum)
        {
            User user = _context.Users.FirstOrDefault(u => u.PersonalAccount == PersonalAccount);
            TransactionToUser transaction = new TransactionToUser()
            {
                FromAccount = user,
                ToAccount = user,
                DateTime = DateTime.Now,
                Sum = Sum
            };
            user.Balanse += Sum;
            _context.Transactions.Add(transaction);
            _context.Users.Update(user);
            _context.SaveChanges();
            return View("Replenishment");
        }
    }
}
