﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;

namespace MoneyTransfer.Controllers
{
    public class TransactionToUsersController : Controller
    {

        private readonly ApplicationContext _context;
        private readonly UserManager<User> _userManager;

        public TransactionToUsersController(ApplicationContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            IEnumerable<TransactionToUser> transactions = _context.Transactions.Include(t => t.FromAccount).Include(t => t.ToAccount).ToList();

            return View("Index", transactions);
        }

        [Authorize]
        public async Task<IActionResult> Create()
        {
            User currentUser = await _userManager.FindByEmailAsync(User.Identity.Name);
            ViewBag.PersonalAccount = currentUser.PersonalAccount;
            return View("Create");
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TransactionToUser transaction)
        {
          
                transaction.DateTime = DateTime.Now;
                _context.Add(transaction);
                User Touser = _context.Users.FirstOrDefault(u => u.PersonalAccount == transaction.ToAccount.PersonalAccount);
                User Fromuser = _context.Users.FirstOrDefault(u => u.PersonalAccount == transaction.FromAccount.PersonalAccount);
                Touser.Balanse += transaction.Sum;
                Fromuser.Balanse -= transaction.Sum;
                await _context.SaveChangesAsync();
            return View("Details",transaction);
        }
    }
}