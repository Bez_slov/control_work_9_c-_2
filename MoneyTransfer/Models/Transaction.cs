﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class TransactionToUser
    {
        [Key]
        public int Id { get; set; }  

        public User FromAccount { get; set; } 
        public User ToAccount { get; set; }   
        public DateTime DateTime { get; set; }     
        public double Sum { get; set; }
    }
}
